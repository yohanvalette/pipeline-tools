#!/bin/bash
get_feature_flag() {
    project_id="$1"
    feature_flag="$2"
    scope="$3"
    environment="$4"

    if [[ "$scope" != "enabled" && "$scope" != "disabled" ]]; then scope=existing; fi
    

    json=$(curl -s --header "PRIVATE-TOKEN: $FEATURE_FLAG_TOKEN" "$CI_API_V4_URL/projects/$project_id/feature_flags/$feature_flag")
    name=$(echo $json | jq -r '.name')

    if [ "$name" == null ]; then echo "2"; return 1; fi

    local result=""
    case $scope in
        enabled)
            result=$(echo $json | jq -rc '. | select(.active == true)')
            ;;
        disabled)
            result=$(echo $json | jq -rc '. | select(.active != true)')
            ;;
        *)
            result=$(echo $json)
    esac

    if [ -n "$environment" ]; then
        result=$(echo $result | jq -rc --arg ENVIRONMENT "$environment" $'.strategies?.scopes[] | select(.environment_scope=$ENVIRONMENT)')
    fi

    echo $result
}

has_feature_flag() {
    project_id="$1"
    feature_flag="$2"
    scope="$3"
    environment="$4"

    json=$(get_feature_flag "$project_id" "$feature_flag" "$scope" $environment | jq -r '.? | select(.name)')

    if [ "$json" == "" ]; then echo "1";
    else echo "0"; fi
}

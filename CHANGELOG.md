# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.3.2] - 2023-11-16

**Fixed**

- Fix versionning

## [1.3.1] - 2022-02-01

**Fixed**

- Fix `get_feature_flag` for environment in `feature_flags.sh`

## [1.3.0] - 2022-01-31

**Added**

- Add `feature_flags.sh` script to get and test feature flags

## [1.2.0] - 2022-01-01

**Added**

- Add `alpine` Dockerfile to build an alpine distribution to the container registry
- Add README.md documentation

## [1.1.0] - 2021-12-31

**Added**

- Add `.version_keep_n_tags` job to cleanup git tags

## [1.0.1] - 2021-12-31

**Fixed**

- Fix tag overwrite with `VERSION_OVERWRITE`

## [1.0.0] - 2021-12-31

**Changed**

- Prepend tags with 'v'

**Fixed**

- Allow rewrite tags for minor, major and latest tags

## [0.2.0] - 2021-12-31

**Added**

- Add `.version_tag` job to create git tags (latest, major, minor and patch)
- Update the pipeline to create latest, major and minor tags

## [0.1.0] - 2021-12-30

**Added**

- Add registry.yml with a job to delete an image on a registry
- Add version.yml with a job to get version from a changelog file and release a version in Gitlab
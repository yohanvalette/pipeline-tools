# Pipeline Tools

## Getting started

This project provides helper tools for continuous integration and deployment through different features:

- Gitlab CI pipeline jobs: a library of jobs that can be easily used
- Scripts: useful scripts to execute in pipeline
- Docker images: common container images that facilitate pipelines

## Pipeline jobs

The pipeline jobs are _hidden_ jobs that can be extended in jobs. The jobs are gathered by categories in files in the `jobs` folder

### Versionning (`version.yml`)

Include the `jobs/version.yml` file:

```yaml
include:
  - project: yohanvalette/pipeline-tools
    file: jobs/version.yml
    ref: v1
```

#### Get version from `CHANGELOG.md`

Extend `.version_from_changelog`

**Description:**

Gather version information from the CHANGELOG.md file. If not the default branch, the version is appended the "-rc" for release candidate

**Requirement:**

- `VERSION_INDEX` : Header2 index - Default = 0
- `DEFAULT_BRANCH` : The default branch - Default = $CI_DEFAULT_BRANCH

**Ouputs:**

- `$VERSION` : Environment variable
- `version.md` : File with the description of the version

#### Create a gitlab release (tag + release)

Extend `.version_release`

**Description:**

Tag and release in Gitlab

**Requirement:**

- Dependency on a job with `$VERSION` variable and `./version.md` artifact

#### Create a version tag

Extend `.version_tag`

**Description:**

Create and push a tag in Gitlab in version

**Requirements:**

- `TAG_USER_NAME` : A user name allowed to create git tags
- `TAG_USER_TOKEN` : A TAG_USER_NAME token
- `VERSION_LEVEL` : latest / major / minor / patch (Default)
- `VERSION_OVERWRITE` : Overwrite tag if it exists. Default: 'false'

**Outputs:**

Creates a tag prepended with 'v'

#### Cleanup git tags

Extend `.version_keep_n_tags`

**Description:**

Cleanup git tags to keep a number of tags matching a pattern

**Requirements:**

- `KEEP_N_TAGS` : Number of tags to keep. Default: 10
- `TAGS_PATTERN` : Regexp tags selector

